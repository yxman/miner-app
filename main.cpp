#include <memory>
#include <yxlib.h>
#include "src/RequestHandler.h"
#include "src/CommunicationService.h"
#include "src/MinerDB.h"
#include <yxlib/DBManager.h>
#include <zmq.hpp>

constexpr auto configFile = "dev-config/config.yaml";

int main() {
    yx::ConfigHelper::addFile(configFile);

    CommunicationService communicationService = CommunicationService::instance();
    yx::DBManager db;

    MinerDB::init(&db);

    RequestHandler requestHandler;

    bool isRunning = true;

    while(isRunning) {

      communicationService.waitForMessage();
      const auto request = communicationService.getServerRequest();
      if (request.type != EMPTY) {
        yx::cout() << "Type du message: \"" << request.type <<  "\""  << std::endl;
        std::string result;
        switch (request.type) {
            case COURSE:
               result = requestHandler.getCourse(request.params);
              break;
            case LOGS:
              result = requestHandler.getLogs(request.params);
              break;
            case PING:
                result = "PONG";
                break;
            case BLOCKCHAIN:
                result = requestHandler.getBlockchain(request.params);
                break;
            case EXIT:
              isRunning = false;
              break;
            default:
              yx::cwar() << "Mauvais type de requete" << std::endl;
        }
        if(result.empty()) {
            yx::cwar() << "Rien a repondre" << std::endl;
        }
        communicationService.sendServerReply(result);

      }
      const auto transaction = communicationService.getServerTransaction();
      if(!transaction.empty()){
        requestHandler.processTransaction(transaction);
      }

       auto block = communicationService.getFoundBlock();
       if (block.block != nullptr) {
           requestHandler.synchronizeBlock(block);
       }

    }
    return EXIT_SUCCESS;
}
