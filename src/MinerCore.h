
#ifndef MINER_APP_MINERCORE_H
#define MINER_APP_MINERCORE_H

#include "Blockchain.h"
#include "CommunicationService.h"
#include "Hasher.h"
#include <memory>

constexpr auto  STARTING_DIFFICULTY = 0;
constexpr auto  TARGET_MINE_TIME_MS = 200;
constexpr auto  DEFAULT_ID = 0;
constexpr auto  DEFAULT_MINER_COUNT = 1;

enum BlockStatus{
    INVALID=-1, CURRENT_BLOCK, PREVIOUS_BLOCK
};

struct MinerState{
    size_t lastMessage;
    std::unique_ptr<Blockchain> blockchain;
};

class MinerCore {
public:
  MinerCore();
  MinerCore(const std::string &hasherType);
  MinerCore(size_t id, size_t totalMiners , const std::string &hasherType = DEFAULT_DIGEST);
  bool addData(const std::string &data);
  bool isValidBlock(std::shared_ptr<Block> block);
  size_t getLength();
  bool hasValidChain();
  std::string getTransaction(size_t id);
  BlockStatus synchronizeBlock(CommunicationService::ReceivedBlock &receivedBlock);
  [[nodiscard]] size_t getId() const;
  [[nodiscard]] std::string getBlockchain(size_t count) const;

private:
  void initializeMiner(std::string hasherType);
  void loadLocalState();
  void processMissedMessages();
  void initGenesysBlock();
  std::shared_ptr<Block> findHash(std::shared_ptr<Block> &block);
  void adjustDifficulty(double elapsedTime);
  bool addBlock(std::shared_ptr<Block>& block);


  size_t _currentDifficulty;
  size_t _id;
  size_t _totalActiveMiners;
  std::string _hasherType;
  MinerState _state;
};

#endif // MINER_APP_MINERCORE_H
