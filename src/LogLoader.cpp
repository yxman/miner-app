
#include "LogLoader.h"
#include <vector>
#include <yxlib.h>

std::string LogLoader::get(size_t id) {
    std::string logs;
    auto section = yx::ConfigHelper::getSection("yxstream");
    std::string logPath = section.getStr("logPath");
    logPath += "access.log";

    std::ifstream inFile(logPath);
    if (inFile.good()) {
        size_t current_line = 0;
        std::string currentLog;
        while(current_line++ < id && getline(inFile, currentLog)){}

        yx::Rest::Json information;
        while(getline(inFile, currentLog)){
            Log log = ParseLine(currentLog);
            yx::Rest::Json node;
            node["no"] = current_line++;
            node["severite"] = log.type;
            node["heure"] = log.time;
            node["message"] = log.source + " - " + log.message;

            information["information"].emplace_back(node);
        }
        logs = information.dump();
    }
    return logs;
}

Log LogLoader::ParseLine(const std::string &logLine) {
    Log log;

    std::stringstream stream(logLine);

    uint64_t timetmp;

    // Get Timestamp
    stream >> timetmp;
    log.timeStamp = timetmp;
    stream.ignore(3, '[');
    stream >> log.type >> log.time >> log.source;
    stream.ignore(3, ']');
    std::getline(stream, log.message);

    return log;
}