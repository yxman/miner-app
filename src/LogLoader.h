
#ifndef MINER_APP_LOGLOADER_H
#define MINER_APP_LOGLOADER_H

#include <string>
#include <boost/fusion/container/vector.hpp>

struct Log {
    time_t timeStamp;
    std::string type;
    std::string time;
    std::string source;
    std::string message;
};

class LogLoader {
public:
    static std::string get(size_t id);
    static Log ParseLine(const std::string &logLine);
};


#endif //MINER_APP_LOGLOADER_H
