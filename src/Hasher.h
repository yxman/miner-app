
#ifndef MINER_APP_HASHER_H
#define MINER_APP_HASHER_H

#include <string>
constexpr auto DEFAULT_DIGEST = "sha512";

class Hasher {
public:
  static std::string hash(const std::string& stringToHash, const std::string& type = DEFAULT_DIGEST);
};

#endif // MINER_APP_HASHER_H
