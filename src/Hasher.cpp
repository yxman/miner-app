
#include "Hasher.h"
#include <iomanip>
#include <iostream>
#include <openssl/evp.h>

std::string Hasher::hash(const std::string& stringToHash, const std::string& type){
  std::stringstream hashedSS;

  EVP_MD_CTX *mdctx;
  unsigned char md_value[EVP_MAX_MD_SIZE];
  unsigned int md_len;

  mdctx = EVP_MD_CTX_create();
  const EVP_MD * digestFct = EVP_get_digestbyname(type.c_str());
  EVP_DigestInit_ex(mdctx, digestFct, nullptr);
  EVP_DigestUpdate(mdctx, stringToHash.c_str(), stringToHash.length());
  EVP_DigestFinal_ex(mdctx, md_value, &md_len);
  EVP_MD_CTX_destroy(mdctx);

  hashedSS << std::hex << std::setw(2) << std::setfill('0');
  for (unsigned int i = 0; i < md_len; i++) {
    hashedSS << (int)md_value[i];
  }
  hashedSS << 0;

  return hashedSS.str();
}
