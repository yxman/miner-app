#ifndef MINER_APP_BLOCKCHAINLOADER_H
#define MINER_APP_BLOCKCHAINLOADER_H

#include "MinerCore.h"

class BlockchainLoader {
public:

    BlockchainLoader() = default;
    static MinerState loadBlockchain();
    static bool saveBlockchain(const MinerState &state);
};


#endif //MINER_APP_BLOCKCHAINLOADER_H
