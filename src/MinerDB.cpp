//
// Created by thephosphorus on 11/7/19.
//

#include "MinerDB.h"

#include <yxlib/ConfigHelper.h>

using namespace yx;

std::unique_ptr<MinerDB> MinerDB::_instance;

void MinerDB::init(yx::DBManager *dbManager) {
    MinerDB &instance = getInstance();
    instance._dbManager = dbManager;

    ConfigSection section = ConfigHelper::getSection("miner");

    instance.tableName = "miner" + std::to_string(section.getUint("id"));

    instance._dbManager->sql() << "CREATE TABLE IF NOT EXISTS " +
                                  instance.tableName +
                                  " (code varchar(10),"
                                  "  trimester varchar(25),"
                                  "  hash VARCHAR(1024),"
                                  "  id INT)";
}

MinerDB &MinerDB::getInstance() {
    if (!_instance) {
        _instance = std::unique_ptr<MinerDB>(new MinerDB());
    }

    return *_instance;
}

void MinerDB::add(const std::string &code, const std::string &trimester,
                  size_t id, const std::string &hash) {
    if (!exists(code, trimester)) {
        _dbManager->sql() << "INSERT INTO " + tableName +
                             " (code, trimester,hash,id) "
                             " VALUES(:code, :trimester, :hash, :id)",
                soci::use(code), soci::use(trimester), soci::use(hash), soci::use(id);
    } else {
        _dbManager->sql() << "UPDATE " + tableName +
                             " SET hash = :hash , id = :id"
                             " WHERE code = :code and trimester = :trimester",
                soci::use(hash), soci::use(id), soci::use(code), soci::use(trimester);
    }
}

std::vector<std::pair<size_t, std::string>>
MinerDB::getGrades(const std::string &code) {
    std::vector<std::pair<size_t, std::string>> arr;

    soci::rowset<soci::row> rs =
            (_dbManager->sql().prepare
                    << "SELECT id, hash from " + tableName + " WHERE code = :code",
                    soci::use(code));

    for (auto &it : rs) {
        soci::row const &row = it;
        arr.emplace_back(
                std::make_pair(row.get<size_t>(0), row.get<std::string>(1)));
    }

    return arr;
}

bool MinerDB::exists(const std::string &code, const std::string &trimester) {
    std::string gottenCode;
    soci::indicator ind;
    _dbManager->sql() << "SELECT code FROM " + tableName +
                         " WHERE code = :code AND trimester = :trimester",
            soci::use(code), soci::use(trimester), soci::into(gottenCode, ind);
    return ind == soci::i_ok && !gottenCode.empty();
}

std::pair<size_t, std::string> MinerDB::getGrade(const std::string &code,
                                                 const std::string &trimester) {
    size_t id = -1;
    std::string hash;
    _dbManager->sql() << "SELECT id, hash FROM " + tableName +
                         " WHERE code = :code AND trimester = :trimester",
            soci::into(id), soci::into(hash), soci::use(code), soci::use(trimester);

    return std::make_pair(id, hash);
}

std::vector<std::pair<size_t, std::string>>
MinerDB::getGrade(const std::string &code) {
    size_t nbTrimesters = 0;

    _dbManager->sql() << "SELECT COUNT(*) FROM " + tableName + " WHERE code = :code", soci::use(code), soci::into(
            nbTrimesters);

    std::vector<std::pair<size_t, std::string>> arr;

    if (nbTrimesters != 0) {


        std::vector<std::string> trimesters;
        trimesters.resize(nbTrimesters);

        _dbManager->sql() << "SELECT trimester FROM " + tableName +
                             " WHERE code = :code",
                soci::use(code), soci::into(trimesters);

        for (const std::string &trimester : trimesters) {
            arr.emplace_back(getGrade(code, trimester));
        }

    }
    return arr;
}
