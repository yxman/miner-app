#include <fstream>
#include <iostream>
#include <sstream> //std::stringstream

#include <yxlib/ConfigHelper.h>
#include <yxlib/RestServer.h>
#include "BlockchainLoader.h"

constexpr auto BLOCKCHAIN_FIELD = "Blocks";
constexpr auto LAST_TRANSACTION_FIELD = "last-transactrion";

MinerState BlockchainLoader::loadBlockchain() {
    auto section = yx::ConfigHelper::getSection("miner");
    std::string blockchainPath = section.getStr("blockchainPath");

    std::ifstream inFile(blockchainPath);
    if (inFile.good()) {
        std::stringstream strStream;
        strStream << inFile.rdbuf(); //read the file
        std::string str = strStream.str(); //str holds the content of the file
        std::unique_ptr<Blockchain> blockchain;
        try { // Assume the local json contains a valid blockchain
            auto jsonData = yx::Rest::Json::parse(str);
            auto blockchainJson = jsonData[BLOCKCHAIN_FIELD].dump();
            auto lastMessage = jsonData[LAST_TRANSACTION_FIELD].get<size_t>();
            return {lastMessage, Blockchain::fromJson(blockchainJson)};
        }
        catch (...){
            return {0, std::make_unique<Blockchain>()};
        }
    }
    // If blockchain does not exist, create new
    return {0, std::make_unique<Blockchain>()};
}

bool BlockchainLoader::saveBlockchain(const MinerState &state) {
    auto section = yx::ConfigHelper::getSection("miner");
    std::string blockchainPath = section.getStr("blockchainPath");

    yx::Rest::Json jsonData;
    jsonData[LAST_TRANSACTION_FIELD] = state.lastMessage;
    jsonData[BLOCKCHAIN_FIELD] = yx::Rest::Json::parse(state.blockchain->toJson());

    std::ofstream out(blockchainPath, std::ios::out);
    out << jsonData.dump(1);
    out.close();

    return true;
}
