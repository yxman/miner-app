
#include <yxlib.h>
#include "Blockchain.h"

bool Blockchain::addBlock(Block &block) {

    auto ptrBlock = std::make_shared<Block>(block);
    addBlock(ptrBlock);
    return true; //to-do : verify block
}

bool Blockchain::addBlock(std::shared_ptr<Block> &block) {
    _blockChain.push_back(block);
    return true;
}

std::shared_ptr<Block> Blockchain::getLastBlock() { return _blockChain.back(); }

size_t Blockchain::getLength() { return _blockChain.size(); }

std::shared_ptr<Block> Blockchain::getPrecedingBlock(std::shared_ptr<Block> &block) {
    size_t idx = block->getIdx();
    return idx>0?_blockChain[idx-1]: nullptr;
}

std::shared_ptr<Block> Blockchain::getBlock(size_t id) {
  if(id >= _blockChain.size())
    return nullptr;
  return _blockChain[id];
}

std::string Blockchain::toJson(size_t count) {
    yx::Rest::Json jsonData = yx::Rest::Json::array();
    size_t i = count?_blockChain.size()-count:0;

    for (; i < _blockChain.size(); ++i) {
        jsonData.push_back(yx::Rest::Json::parse(_blockChain[i]->toJson()));
    }

    return jsonData.dump();
}

std::unique_ptr<Blockchain> Blockchain::fromJson(const std::string & json) {
    yx::Rest::Json jsonData = yx::Rest::Json::parse(json);

    std::unique_ptr<Blockchain> blockchain = std::make_unique<Blockchain>();
    for (auto & blockJson : jsonData)
    {
        Block block = Block::fromJson(blockJson.dump());
        blockchain->addBlock(block);
    }

    return blockchain;
}

Blockchain& operator+=(Blockchain &bc1, Blockchain const &bc2) {

    bc1._blockChain.reserve(bc1._blockChain.size() + bc2._blockChain.size());
    bc1._blockChain.insert(bc1._blockChain.end(), bc2._blockChain.begin(),  bc2._blockChain.end());
    return bc1;
}


