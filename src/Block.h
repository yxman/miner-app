
#ifndef MINER_APP_BLOCK_H
#define MINER_APP_BLOCK_H

#include <ctime>
#include <string>

#define NO_HASH "NOHASH"

class Block {
public:
  Block(size_t index, std::string data, std::string previousHash);
  Block(size_t index, std::string data, std::string previousHash, time_t timestamp,
          std::string hash, size_t nonce, size_t validationCount);
  [[nodiscard]] std::string getPreviousHash() const;
  [[nodiscard]] std::string getHash() const;
  [[nodiscard]] std::string getHashableStr() const;
  [[nodiscard]] std::string getData() const;
  [[nodiscard]] std::string toJson() const;
  static Block fromJson(const std::string & json);
  bool setHash(std::string hash);
  void setNonce(size_t value);
  friend std::ostream &operator<<(std::ostream &os, const Block &block);
  bool operator==(const Block &block) const;
  [[nodiscard]] size_t getIdx() const;
  void validate();
  [[nodiscard]] size_t getValidationCount() const;

private:
  size_t _index;
  std::string _hash;
  std::string _previousHash;
  time_t _timestamp;
  std::string _data;
  size_t _nonce;
  size_t _validationCount = 0;
};

#endif // MINER_APP_BLOCK_H
