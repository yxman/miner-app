
#ifndef MINER_APP_COMMUNICATIONSERVICE_H
#define MINER_APP_COMMUNICATIONSERVICE_H

#include <yxlib.h>
#include "Block.h"
#include "Blockchain.h"
#include <vector>
#include <memory>
#include <zmq.hpp>

constexpr auto ID_FIELD = "id";
constexpr auto BLOCK_FIELD = "block";


class CommunicationService {
public:

    struct ReceivedBlock{
        int source;
        std::shared_ptr<Block> block;
    };

    ~CommunicationService();
    static CommunicationService& instance();
    ReceivedBlock getFoundBlock(size_t selfId = -1);
    std::string getServerTransaction();
    yx::CommHelper::Request getServerRequest();
    bool sendServerReply(const std::string& reply);
    bool sendFoundBlock(std::shared_ptr<Block> &block, size_t source);
    void waitForMessage();
    std::vector<ReceivedBlock> getMissingMinerMessages(size_t lastMessageId);

private:
    CommunicationService();
    void initializeConnections();
    static ReceivedBlock parseBlockString(const std::string &message, size_t selfId = -1);


private:
    std::shared_ptr<zmq::context_t> _context;
    std::shared_ptr<zmq::socket_t> _serverTransactionsSubscriber;
    std::shared_ptr<zmq::socket_t> _serverRequestSubscriber;
    std::shared_ptr<zmq::socket_t> _minerPublisher;
    std::shared_ptr<zmq::socket_t> _minerSubscriber;
    std::shared_ptr<zmq::socket_t> _cacheRequest;
    static std::unique_ptr<CommunicationService> _instance;


};


#endif //MINER_APP_COMMUNICATIONSERVICE_H
