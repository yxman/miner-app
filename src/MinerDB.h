//
// Created by thephosphorus on 11/7/19.
//

#ifndef MINER_APP_MINERDB_H
#define MINER_APP_MINERDB_H

#include <memory>
#include <vector>
#include <yxlib/DBManager.h>

class MinerDB {
public:
  static MinerDB &getInstance();
  static void init(yx::DBManager *dbManager);

  std::vector<std::pair<size_t, std::string>>
  getGrades(const std::string &code);

  std::pair<size_t, std::string> getGrade(const std::string& code, const std::string& trimester);

  void add(const std::string &code, const std::string &trimester, size_t id,
           const std::string &hash);

  bool exists(const std::string& code, const std::string& trimester);

  [[nodiscard]] std::vector<std::pair<size_t, std::string>> getGrade(const std::string& code);
private:
  MinerDB() = default;

private:
  static std::unique_ptr<MinerDB> _instance;
  yx::DBManager *_dbManager;
  std::string tableName;
};

#endif // MINER_APP_MINERDB_H
