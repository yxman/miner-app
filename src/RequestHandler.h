
#ifndef MINER_APP_REQUESTHANDLER_H
#define MINER_APP_REQUESTHANDLER_H

#include <string>
#include <memory>
#include "MinerCore.h"


class RequestHandler {
public:
    explicit RequestHandler();
    int processTransaction(const std::string &params);
    std::string getCourse(const std::string &params);
    std::string getLogs(const std::string &params);
    std::string getBlockchain(const std::string &params);
    int synchronizeBlock(CommunicationService::ReceivedBlock &block);


private:
    std::unique_ptr<MinerCore> _mc;
};


#endif //MINER_APP_REQUESTHANDLER_H
