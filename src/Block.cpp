#include "Block.h"
#include <ctime>
#include "../lib/yxlib/include/yxlib.h"
#include <sstream>
#include <utility>

constexpr auto INDEX_FIELD = "index";
constexpr auto HASH_FIELD = "hash";
constexpr auto TIMESTAMP_FIELD = "timestamp";
constexpr auto PREVIOUS_HASH_FIELD = "prevHash";
constexpr auto DATA_FIELD = "data";
constexpr auto NONCE_FIELD = "nonce";
constexpr auto VALIDATION_FIELD = "validation";

Block::Block(size_t index, std::string data, std::string previousHash)
    : _index(index), _data(std::move(data)),
      _previousHash(std::move(previousHash)), _hash(NO_HASH),
      _nonce(0){
  _timestamp = time(nullptr);
}

Block::Block(size_t index, std::string data, std::string previousHash,
             time_t timestamp, std::string hash, size_t nonce, size_t validationCount):_index(index),
                              _data(std::move(data)),
                              _previousHash(std::move(previousHash)), _hash(std::move(hash)),
                              _nonce(nonce), _timestamp(timestamp),
                              _validationCount(validationCount){}

std::ostream &operator<<(std::ostream &os, const Block &block) {
  os << block._nonce << block._index << block._timestamp << block._data << block._previousHash;
  return os;
}

std::string Block::getPreviousHash() const { return _previousHash; }

std::string Block::getHash() const { return _hash; }
std::string Block::getHashableStr() const {
  std::stringstream ss;
  ss << *this;
  return ss.str();
}

bool Block::setHash(std::string hash) {
  if (_hash != NO_HASH) {
    yx::cwar() << "On ne peut pas remplacer un hash existant" << std::endl;
    return false;
  }

  _hash = std::move(hash);
  return true;
}

void Block::setNonce(size_t value) { _nonce=value; }

bool Block::operator==(const Block &block) const {
  return this->_hash == block._hash;
}

size_t Block::getIdx() const {
    return _index;
}
std::string Block::getData() const {
  return _data;
}

std::string Block::toJson() const {

  yx::Rest::Json jsonData;
  jsonData[INDEX_FIELD] = _index;
  jsonData[HASH_FIELD] = _hash;
  jsonData[TIMESTAMP_FIELD] = _timestamp;
  jsonData[PREVIOUS_HASH_FIELD] = _previousHash;
  jsonData[DATA_FIELD] = _data;
  jsonData[NONCE_FIELD] = _nonce;
  jsonData[VALIDATION_FIELD] = _validationCount;

  return jsonData.dump();
}

Block Block::fromJson(const std::string & json) {
  yx::Rest::Json jsonData = yx::Rest::Json::parse(json);
  auto id = jsonData[INDEX_FIELD].get<size_t>();
  auto hash = jsonData[HASH_FIELD].get<std::string>();
  auto prevHash = jsonData[PREVIOUS_HASH_FIELD].get<std::string>();
  auto ts = jsonData[TIMESTAMP_FIELD].get<time_t>();
  auto data = jsonData[DATA_FIELD].get<std::string>();
  auto nonce = jsonData[NONCE_FIELD].get<size_t>();
  auto validationCount = jsonData[VALIDATION_FIELD].get<size_t>();
  return Block(id, data, prevHash, ts, hash, nonce,validationCount);// validationCount);
}

void Block::validate() {
    _validationCount++;
}

size_t Block::getValidationCount() const {
    return _validationCount;
}
