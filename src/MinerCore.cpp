#include "MinerCore.h"
#include "Hasher.h"
#include "CommunicationService.h"
#include "BlockchainLoader.h"
#include <chrono>
#include <utility>

MinerCore::MinerCore():_id(DEFAULT_ID),_totalActiveMiners(DEFAULT_MINER_COUNT) {
  initializeMiner(DEFAULT_DIGEST);
}

MinerCore::MinerCore(const std::string &hasherType): _id(DEFAULT_ID), _totalActiveMiners(DEFAULT_MINER_COUNT) {
  initializeMiner(hasherType);
}

MinerCore::MinerCore(size_t id, size_t totalMiners, const std::string &hasherType): _id(id), _totalActiveMiners(totalMiners) {
  initializeMiner(hasherType);
}

void MinerCore::initializeMiner(std::string hasherType) {
    _hasherType = std::move(hasherType);
    _currentDifficulty = STARTING_DIFFICULTY;

    yx::cout() << "Initialisation du mineur" << std::endl;

    loadLocalState();

    processMissedMessages();

    initGenesysBlock();

    yx::cout() << "initialisation du mineur termine" << std::endl;
}

void MinerCore::loadLocalState() {
    _state = BlockchainLoader::loadBlockchain();
    yx::cout() << "Recherche d'un blockchain local" << std::endl;
    if (_state.blockchain->getLength() != 0) {
        yx::cout() << "Blockchain local de taille " << _state.blockchain->getLength() << " trouve et charge" << std::endl;
        auto lastBlock = _state.blockchain->getLastBlock();
        while (lastBlock->getHash()[_currentDifficulty] == '0')
            _currentDifficulty++;
        if(!hasValidChain()){
            yx::cwar() << "Le blockchain charge n'est pas valide" << std::endl ;
            yx::cwar() << "Reinitialisation du blockchain" << std::endl;
            _state = {0, std::make_unique<Blockchain>()};
        }
    }else{
        yx::cout() << "Aucun blockchain local trouve" << std::endl;
    }
}


void MinerCore::processMissedMessages() {
    yx::cout() << "Demande a la cache des requetes manquantes" << std::endl;
    auto &comm = CommunicationService::instance();
    auto missingMessages = comm.getMissingMinerMessages(_state.lastMessage);

    if (!missingMessages.empty()) {
        for(auto& block:missingMessages){
            auto status = synchronizeBlock(block);
            if(status == BlockStatus::CURRENT_BLOCK && block.source != _id)
                comm.sendFoundBlock(block.block, _id);
        }
        yx::cout() << "Fin des requetes manquantes" << std::endl;
    }else{
        yx::cout() << "Aucune requete manquante" << std::endl;
    }
}

void MinerCore::initGenesysBlock() {
    if (_state.blockchain->getLength() == 0) {
        std::string genBlock = "GenesysBlock";
        yx::cout() << "Blockchain existant non trouve: generation du premier bloc" << std::endl;
        addData(genBlock);
    }
}

bool MinerCore::addData(const std::string &data) {

  const size_t nextIndex = _state.blockchain->getLength();
  const std::string previousHash =
      nextIndex == 0 ? NO_HASH : _state.blockchain->getLastBlock()->getHash();
  auto block = std::make_shared<Block>(nextIndex, data, previousHash);
  std::shared_ptr<Block> minedBlock = findHash(block);

  if(minedBlock == nullptr)
      return false;
  
  return CommunicationService::instance().sendFoundBlock(minedBlock, _id);
}


bool MinerCore::isValidBlock(std::shared_ptr<Block> block) {
    if (block->getHash() != Hasher::hash(block->getHashableStr())) {
        yx::cwar() << "Le block contient un hash invalide" << std::endl;
        return false;
    }
    std::shared_ptr<Block> previousBlock = _state.blockchain->getPrecedingBlock(block);
    if (previousBlock == nullptr){
        if (block->getPreviousHash() != NO_HASH)
            return false;
    }
    else if(previousBlock->getHash() != block->getPreviousHash())
        return false;

    return true;
}

bool MinerCore::hasValidChain() {

  std::shared_ptr<Block> currentBlock = _state.blockchain->getLastBlock();
  do{
    if(isValidBlock(currentBlock))
      currentBlock = _state.blockchain->getPrecedingBlock(currentBlock);
    else
      return false;
  }while(currentBlock != nullptr);

  return true;
}

std::string MinerCore::getTransaction(size_t id) {
  auto block = _state.blockchain->getBlock(id);
  if(block!= nullptr && isValidBlock(block)){
      return block->getData();
  }
  return "";
}

std::shared_ptr<Block> MinerCore::findHash(std::shared_ptr<Block> &block) {
  size_t nonce = _id;
  std::string hash;
  const std::string prefix = std::string(_currentDifficulty, '0');

  using clock = std::chrono::system_clock;
  using ms = std::chrono::duration<double, std::milli>;

  yx::cout() << "Début de minage du block " << std::endl;

  const auto start_time = clock::now();
  auto& comm = CommunicationService::instance();
  do {

    auto foundBlock = comm.getFoundBlock(_id);
    if (foundBlock.block != nullptr) {
        if (synchronizeBlock(foundBlock) == BlockStatus::CURRENT_BLOCK){
            return foundBlock.block;
        }
    }

    block->setNonce(nonce);
    nonce += _totalActiveMiners;
    hash = Hasher::hash(block->getHashableStr(), _hasherType);

  }while(hash.substr(0, _currentDifficulty) != prefix);

  const ms duration = clock::now() - start_time;
  yx::cout() << "Le bloc a été miné après "<< duration.count() << " ms" << std::endl;
  yx::cout() << "Le nounce est : " << nonce << std::endl;
  yx::cout() << "Le hash est : " << hash << std::endl;

  adjustDifficulty(duration.count());

  if (!block->setHash(hash)){
    yx::cerr() << "Le hash n'a pas pu être ajouté" << std::endl;
    return nullptr;
  }

  return block;
}

void MinerCore::adjustDifficulty(double elapsedTimeMs){
  const float difficultyVariation =  float(elapsedTimeMs) / TARGET_MINE_TIME_MS;
  if(difficultyVariation < 1.0/16.0) {
    yx::cout() << "Augmentation de la difficulté de minage" << std::endl;
    _currentDifficulty++;
  }
  else if(difficultyVariation > 16 && _currentDifficulty > 0) {
    yx::cout() << "Diminution de la difficulté de minage" << std::endl;
    _currentDifficulty--;
  }
}
BlockStatus MinerCore::synchronizeBlock(CommunicationService::ReceivedBlock &receivedBlock) {
    _state.lastMessage++;
    auto& block = receivedBlock.block;

    yx::cout() << "Block " << block->getIdx() << " reçu par le mineur "<< receivedBlock.source << " avec un hash de : " << block->getHash() << std::endl;

    if(isValidBlock(block)) {
        if (block->getIdx() == _state.blockchain->getLength()) {
            yx::cout() << "Le bloc a ete rajoute a la chaine" << std::endl;
            block->validate();
            addBlock(block);
            return BlockStatus::CURRENT_BLOCK;
        }

        auto localBlock = _state.blockchain->getBlock(block->getIdx());
        if (*localBlock == *block) {
            localBlock->validate();
            yx::cout() << "Le bloc reçu a valider le block local " << localBlock->getIdx() << " qui est valider par : "
                       << localBlock->getValidationCount() << "/" << _totalActiveMiners << " mineurs" << std::endl;
            return BlockStatus::PREVIOUS_BLOCK;
        }
    }

  yx::cwar() << "Le bloc reçu est invalide" << std::endl;
  return BlockStatus::INVALID;
}

size_t MinerCore::getLength() {
  return _state.blockchain->getLength();
}


bool MinerCore::addBlock(std::shared_ptr<Block> &block) {
    bool isSuccess =  _state.blockchain->addBlock(block);
    if(isSuccess) {
        BlockchainLoader::saveBlockchain(_state);
    }
    return isSuccess;
}

size_t MinerCore::getId() const {
    return _id;
}

std::string MinerCore::getBlockchain(size_t count) const {
    return _state.blockchain->toJson(count);
}

