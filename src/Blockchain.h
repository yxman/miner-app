
#ifndef MINER_APP_BLOCKCHAIN_H
#define MINER_APP_BLOCKCHAIN_H

#include "Block.h"
#include <memory>
#include <vector>

class Blockchain {
private:
  std::vector<std::shared_ptr<Block>> _blockChain;

public:
    Blockchain() = default;
    bool addBlock(Block& block);
    bool addBlock(std::shared_ptr<Block>& block);
    std::shared_ptr<Block> getBlock(size_t id);
    std::shared_ptr<Block> getLastBlock();
    std::shared_ptr<Block> getPrecedingBlock(std::shared_ptr<Block>& block);
    size_t getLength();
    std::string toJson(size_t count = 0);
    static std::unique_ptr<Blockchain> fromJson(const std::string & json);
    friend Blockchain& operator+=(Blockchain &bc1, Blockchain const& bc2);
};

#endif // MINER_APP_BLOCKCHAIN_H
