#include "RequestHandler.h"
#include "MinerDB.h"
#include "LogLoader.h"
#include "yxlib.h"


RequestHandler::RequestHandler() {
  auto section = yx::ConfigHelper::getSection("miner");
  size_t id = section.getUint("id");
  size_t nMiners = section.getUint("nMiners");
  _mc = std::make_unique<MinerCore>(id, nMiners);
}

int RequestHandler::processTransaction(const std::string &params) {
    //todo : Use the minor and database to process the transaction and return SUCCESS or Failure
    auto db = MinerDB::getInstance();
    try {
      auto jsonParams = yx::Rest::Json::parse(params);
      Transaction transaction = jsonToTransaction(jsonParams);
      db.add(transaction.id,std::to_string(transaction.trimester), _mc->getLength(), NO_HASH);
    }catch(...){
      db.add("MOCK","MOCK", _mc->getLength(), NO_HASH);
    }

    bool success = _mc->addData(params);

    if(!success)
      return -1;

    return 0;
}

std::string RequestHandler::getCourse(const std::string &params) {
    //todo : Use the minor and database to process the transaction and return SUCCESS or Failure
    auto jsonParams = yx::Rest::Json::parse(params);
    CourseRequest courseRequest = jsonToCourseRequest(jsonParams);
    yx::Rest::Json transactionList;

    if(courseRequest.trimester == -1){
        auto blockIdentifiersList = MinerDB::getInstance().getGrade(courseRequest.id);
        for(const auto& blockIdentifiers:blockIdentifiersList){
            MinerDB::getInstance().getGrade(courseRequest.id, std::to_string(courseRequest.trimester));
            size_t id = blockIdentifiers.first;
            std::string dbHash = blockIdentifiers.second;
            if (id != -1) {
              transactionList.push_back(
                  yx::Rest::Json::parse(_mc->getTransaction(id)));
            }
        }
    }else{
        auto blockIdentifiers = MinerDB::getInstance().getGrade(courseRequest.id, std::to_string(courseRequest.trimester));
        size_t id = blockIdentifiers.first;
        std::string dbHash = blockIdentifiers.second;
        if (id != -1 && !_mc->getTransaction(id).empty()) {
          transactionList.push_back(yx::Rest::Json::parse(_mc->getTransaction(id)));
        }
    }

    return ((transactionList.empty())? "" : transactionList.dump());
}


std::string RequestHandler::getLogs(const std::string &params) {

    yx::Rest::Json jsonData = yx::Rest::Json::parse(params);
    if (jsonData.contains("dernier")) {
        yx::Rest::Json response;
        response["id"] = _mc->getId();

        std::string logs = LogLoader::get(jsonData["dernier"]);

        response["logs"] = yx::Rest::Json::parse(logs);

        return response.dump();
    }
    yx::cwar() << "erreur dans le format de la requete" << std::endl;
    return "";
}

int RequestHandler::synchronizeBlock(CommunicationService::ReceivedBlock &block) {
    BlockStatus status = _mc->synchronizeBlock(block);
    if(status == BlockStatus::INVALID){
        return -1;
    }

    if(status == BlockStatus::CURRENT_BLOCK && block.source != _mc->getId()){
        CommunicationService::instance().sendFoundBlock(block.block, _mc->getId());
        return 1;
    }

    return 0;
}

std::string RequestHandler::getBlockchain(const std::string &params) {
    yx::Rest::Json jsonData = yx::Rest::Json::parse(params);
    if (jsonData.contains("derniers_blocs")) {
        yx::Rest::Json response;
        response["id"] = _mc->getId();
        response["blockchain"] = _mc->getBlockchain(jsonData["derniers_blocs"]);
        return response.dump();
    }
    yx::cwar() << "erreur dans le format de la requete" << std::endl;


    return "";
}
