#include "CommunicationService.h"
#include <memory>
#include <zmq.hpp>

std::unique_ptr<CommunicationService> CommunicationService::_instance;

CommunicationService &CommunicationService::instance() {

  if(!_instance){
      _instance = std::unique_ptr<CommunicationService>(new CommunicationService());
  }

  return *_instance;
}

CommunicationService::CommunicationService() {
  initializeConnections();
}

void CommunicationService::initializeConnections() {
  const auto communication = yx::ConfigHelper::getSection("communication");

  std::string connectionPrefix = "tcp://";
  _context = std::make_shared<zmq::context_t>(1);

  const auto server = communication.getConfig("server");
  std::string serverIp = server.getStr("ip");

  std::string transactionPort = server.getStr("transactionPort");
  _serverTransactionsSubscriber = yx::CommHelper::initConnection(_context, connectionPrefix + serverIp + ':' + transactionPort, zmq::socket_type::sub, false);

  std::string getterPort = server.getStr("getterPort");
  _serverRequestSubscriber = yx::CommHelper::initConnection(_context, connectionPrefix + serverIp + ':' + getterPort, zmq::socket_type::rep, false);

  const auto proxy = communication.getConfig("proxy");
  std::string proxyIp = proxy.getStr("ip");
  const auto proxyPorts = proxy.getConfig("ports");

  std::string publisherPort = proxyPorts.getStr("publisher");
  _minerPublisher= yx::CommHelper::initConnection(_context, connectionPrefix + proxyIp + ':' + publisherPort,
          zmq::socket_type::pub, false);


  std::string subscriberPort  = proxyPorts.getStr("subscriber");
  _minerSubscriber = yx::CommHelper::initConnection(_context, connectionPrefix + proxyIp + ':' + subscriberPort,
                                                           zmq::socket_type::sub, false);

  std::string cachePort  = proxyPorts.getStr("cache");
  _cacheRequest = yx::CommHelper::initConnection(_context, connectionPrefix + proxyIp + ':' + cachePort,
                                                             zmq::socket_type::req, false);

}


CommunicationService::ReceivedBlock CommunicationService::getFoundBlock(size_t selfId) {

  std::string message = yx::CommHelper::receiveString(_minerSubscriber, zmq::recv_flags::dontwait);

  return parseBlockString(message, selfId);
}

bool CommunicationService::sendFoundBlock(std::shared_ptr<Block> &block, size_t source) {
  yx::cout() << "Envoie du block aux autres mineurs." << std::endl;
  yx::Rest::Json blockJson;
  blockJson["id"] = source;
  blockJson["block"] = block->toJson();
  return yx::CommHelper::sendString(_minerPublisher, blockJson.dump(), zmq::send_flags::dontwait);
}

std::string CommunicationService::getServerTransaction() {
  std::string message = yx::CommHelper::receiveString(_serverTransactionsSubscriber, zmq::recv_flags::dontwait);
  if(!message.empty()) {
      yx::cout() << "Reception de la transaction : " << message << std::endl;
  }
  return message;
}

yx::CommHelper::Request CommunicationService::getServerRequest() {
    return yx::CommHelper::receiveRequest(_serverRequestSubscriber, zmq::recv_flags::dontwait);
}


bool CommunicationService::sendServerReply(const std::string& reply) {
  yx::cout() << "Envoie de la réponse au serveur" << std::endl;
  return yx::CommHelper::sendString(_serverRequestSubscriber, reply);
}

CommunicationService::~CommunicationService() {
    _serverTransactionsSubscriber->close();
    _serverRequestSubscriber->close();
    _minerPublisher->close();
    _minerSubscriber->close();
}

void CommunicationService::waitForMessage() {
    zmq::pollitem_t poller[3];
    poller[0] = {*_serverRequestSubscriber, 0, ZMQ_POLLIN, 0};
    poller[1] = {*_serverTransactionsSubscriber, 0, ZMQ_POLLIN, 0};
    poller[2] = {*_minerSubscriber, 0, ZMQ_POLLIN, 0};
    zmq::poll(poller, 3);
}

std::vector<CommunicationService::ReceivedBlock> CommunicationService::getMissingMinerMessages(size_t lastMessageId) {
    yx::CommHelper::sendString(_cacheRequest, std::to_string(lastMessageId));
    std::string response = yx::CommHelper::receiveString(_cacheRequest);

    std::vector<ReceivedBlock> missedBlocks;

    yx::Rest::Json respJson = yx::Rest::Json::parse(response);
    if(!respJson.contains("messages")){
        yx::cerr() << "Reponse de la cache invalide pour les messages manque" << std::endl;
        return missedBlocks;
    }
    auto missedMessages = respJson["messages"];
    for(auto& message:missedMessages){
        auto block = parseBlockString(message);
        if(block.block != nullptr){
            missedBlocks.push_back(block);
        }
    }

    return missedBlocks;
}

CommunicationService::ReceivedBlock CommunicationService::parseBlockString(const std::string &message, size_t selfId) {
    if(!message.empty()) {
        try{
            auto msgJson = yx::Rest::Json::parse(message);
            int id = msgJson[ID_FIELD];

            if(id == selfId){
                yx::cwar() << "Message ignored because it was from miner " << id << std::endl;
                return {id, nullptr};
            }

            auto block = std::make_shared<Block>(Block::fromJson(msgJson[BLOCK_FIELD]));

            return {id, block};

        }catch(InvalidJsonFormatException& exception){
            yx::cwar() << "Reception d'un block dans le mauvais format" << std:: endl;
        }
    }
    return {-1, nullptr};
}
