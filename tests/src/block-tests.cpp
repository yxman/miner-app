#include "gtest/gtest.h"
#include "../../src/Block.h"


TEST(block_test,class_creation){
    std::string data = "data";
    std::string hash = NO_HASH;
    uint32_t index = 0;
    ASSERT_NO_THROW(Block(index, data, hash));
}

TEST(block_test,set_hash){
    std::string data = "data";
    std::string phash = NO_HASH;
    uint32_t index = 0;
    Block block(index, data, phash);
    std::string hash = "12345";
    block.setHash(hash);
    ASSERT_EQ(block.getHash(), hash);
}

TEST(block_test,fail_invalid_hash){
    std::string data = "data";
    std::string phash = NO_HASH;
    uint32_t index = 0;
    Block block(index, data, phash);
    std::string hash = "12345";
    block.setHash(hash);
    ASSERT_EQ(block.setHash(hash), false);
}
