#include "gtest/gtest.h"
#include "../../src/Hasher.h"


TEST(hasherSha_tests,hash_is_hexa){
    std::string test = "test";
    std::string hash = Hasher::hash(test);
    ASSERT_TRUE(std::all_of(hash.begin(), hash.end(), ::isxdigit));
}


TEST(hasherSha_tests,gives_same_hash){
    std::string test = "test";
    ASSERT_EQ(Hasher::hash(test), Hasher::hash(test));
}
