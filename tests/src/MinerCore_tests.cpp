#include "gtest/gtest.h"
#include "../../src/Hasher.h"
#include "../../src/MinerCore.h"

class MinerCore_test : public testing::Test {
protected:
    void SetUp() override {
        mc=MinerCore();
    }

    MinerCore mc;

};

TEST_F(MinerCore_test,class_creation){
    ASSERT_NO_THROW(MinerCore());
}

TEST_F(MinerCore_test,addData){
    std::string data = "hello";
    ASSERT_NO_THROW(mc.addData(data));
}

TEST_F(MinerCore_test,valid_block){
    std::string data = "hello";
    mc.addData(data);
    std::shared_ptr<Block> block = std::make_shared<Block>(0,data, NO_HASH);
    std::string hash = Hasher::hash(block->getHashableStr());
    block->setHash(hash);
    ASSERT_TRUE(mc.isValidBlock(block));
}

TEST_F(MinerCore_test,invalid_data){
    std::string data = "hello";
    mc.addData(data);
    std::shared_ptr<Block> block = std::make_shared<Block>(0,"poop", NO_HASH);
    ASSERT_FALSE(mc.isValidBlock(block));
}

TEST_F(MinerCore_test,invalid_previous_hash){
    std::string data = "hello";
    std::string data2 = "poop";
    mc.addData(data);
    mc.addData(data2);
    std::shared_ptr<Block> block = std::make_shared<Block>(1,data2, NO_HASH);
    std::string hash = Hasher::hash(block->getHashableStr());
    block->setHash(hash);
    ASSERT_FALSE(mc.isValidBlock(block));
}
