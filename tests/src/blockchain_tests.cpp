#include "gtest/gtest.h"
#include "../../src/Blockchain.h"


TEST(blockchain_test,class_creation){

    ASSERT_NO_THROW(Blockchain());
}

TEST(blockchain_test,add_block){
    std::string data = "data";
    std::string phash = NO_HASH;
    uint32_t index = 0;
    Block block(index, data, phash);
    Blockchain bc;
    ASSERT_NO_THROW(bc.addBlock(block));
    ASSERT_EQ(block,*bc.getLastBlock() );
}

